package chess;

import java.awt.Point;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

import chess.ui.BoardView;
import javafx.geometry.Point2D;
import javafx.scene.layout.Pane;

//Représente la planche de jeu avec les pièces.


public class ChessBoard {

	// Grille de jeu 8x8 cases. Contient des références aux piéces présentes sur
	// la grille.
	// Lorsqu'une case est vide, elle contient une pièce spéciale
	// (type=ChessPiece.NONE, color=ChessPiece.COLORLESS).
	private ChessPiece[][] grid;
	
	private BoardView bv;

	public ChessBoard(int x, int y) {
	    bv = new BoardView(x, y);
		// Initialise la grille avec des pièces vides.
		grid = new ChessPiece[8][8];
		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				grid[i][j] = new ChessPiece(i, j, this);
			}
		}
		
	}
	
    // Place une pièce sur le planche de jeu.
    public void putPiece(ChessPiece piece) {
        Point2D pos = bv.gridToPane(piece, piece.getGridX(), piece.getGridY());
        piece.getUI().relocate(pos.getX(), pos.getY());
        getUI().getChildren().add(piece.getUI());
        grid[piece.getGridX()][piece.getGridY()] = piece;
    }
    
    public ChessPiece getPiece(Point pos) {
    	return grid[pos.x][pos.y];
    }
    
    public void removePiece(Point pos) {
	    getUI().getChildren().remove(grid[pos.x][pos.y].getUI());
    }
    
    public void assignSquare(Point pos, ChessPiece piece) {
    	grid[pos.x][pos.y] = piece;
    	piece.setGridPos(pos);
    }

	// Place une pièce vide dans la case
	public void clearSquare(int x, int y) {
		grid[x][y] = new ChessPiece(x, y, this);
	}
	
	// Place une pièce vide dans la case
	public void clearSquare(Point pos) {
		grid[pos.x][pos.y] = new ChessPiece(pos.x, pos.y, this);
	}

	//Les cases vides contiennent une pièce spéciale
	public boolean isEmpty(Point pos) {
		return (grid[pos.x][pos.y].getType() == ChessUtils.TYPE_NONE);
	}

	//Vérifie si une coordonnée dans la grille est valide
	public boolean isValid(Point pos) {
		return (pos.x >= 0 && pos.x <= 7 && pos.y >= 0 && pos.y <= 7);
	}

	//Vérifie si les pièces à deux positions dans la grille sont de la même couleur.
	public boolean isSameColor(Point pos1, Point pos2) {
		return grid[pos1.x][pos1.y].getColor() == grid[pos2.x][pos2.y].getColor();
	}
	
	//Effectue un mouvement à partir de la notation algébrique des cases ("e2-b5" par exemple)
	public void algebraicMove(String move){
		if(move.length()!=5){
			throw new IllegalArgumentException("Badly formed move");
		}
		String start = move.substring(0,2);
		String end = move.substring(3,5);
		move(ChessUtils.convertAlgebraicPosition(start),ChessUtils.convertAlgebraicPosition(end));
	}
	
	//Effectue un mouvement sur l'échiqier. Quelques règles de base sont implantées ici.
	public boolean move(Point gridPos, Point newGridPos) {
		
		ChessPiece piece = getPiece(gridPos);
		
		//Vérifie si les coordonnées sont valides
		if (!isValid(newGridPos))
			return false;
		
		else if(!piece.verifyMove(gridPos, newGridPos)) {
			return false;
		}

		//Si la case destination est vide, on peut faire le mouvement
		else if (isEmpty(newGridPos)) {
			assignSquare(newGridPos, grid[gridPos.x][gridPos.y]);
			clearSquare(gridPos);
			return true;
		}

		//Si elle est occuppé par une pièce de couleur différente, alors c'est une capture
		else if (!isSameColor(gridPos, newGridPos)) {
			removePiece(newGridPos);
			assignSquare(newGridPos, grid[gridPos.x][gridPos.y]);
			clearSquare(gridPos);
			return true;
		}

		return false;
	}
	
    public void move(Point2D pos, Point2D newPos) {
        Point gridPos = bv.paneToGrid(pos.getX(), pos.getY());
        Point newGridPos = bv.paneToGrid(newPos.getX(), newPos.getY());
        ChessPiece piece = grid[gridPos.x][gridPos.y];
        if(move(gridPos, newGridPos)) {
            piece.move(bv.gridToPane(piece, newGridPos.x, newGridPos.y));
        } else {
            piece.move(bv.gridToPane(piece, gridPos.x, gridPos.y));
        }
    }
	
	public Pane getUI() {
	    return bv.getPane();
	}
	
	//Fonctions de lecture et de sauvegarde d'échiquier dans des fichiers. À implanter.
	public static ChessBoard readFromFile(String fileName) throws Exception {
		return readFromFile(new File(fileName), 0, 0);
	}

	public static ChessBoard readFromFile(File file, int x, int y) throws Exception {
	    Scanner sc;
        try
        {
            sc = new Scanner(file);
            ChessBoard board = new ChessBoard(x, y);
            while (sc.hasNextLine()) {
                board.putPiece(ChessPiece.readFromStream(sc.nextLine(), board));
            }
            sc.close();
            return board;
        }
        catch (FileNotFoundException e)
        {
            throw new Exception("Fichier invalide");
        }
	}
	
	
	public void saveToFile(File file) throws Exception {
	    PrintWriter writer;
        try
        {
            writer = new PrintWriter(file);
            String stream;
            for(ChessPiece[] line : grid) {
                for(ChessPiece piece : line) {
                	stream = piece.saveToStream();
                	if(!stream.isEmpty()){
                        writer.println(stream);
                	}
                }
            }
            writer.close();
        }
        catch (FileNotFoundException e)
        {
            throw new Exception("Fichier invalide");
        }
	}
	
	@Override
    public boolean equals(Object obj) {
		if((obj == null) || (getClass() != obj.getClass())){
            return false;
        }

        final ChessBoard other = (ChessBoard) obj;
        
        for(int i = 0; i < grid.length; i++) {
        	for(int j = 0; j < grid[i].length; j++) {
        		if(!grid[i][j].equals(other.grid[i][j])) {
        			return false;
        		}
            }
        }

        return true;
    }
}
