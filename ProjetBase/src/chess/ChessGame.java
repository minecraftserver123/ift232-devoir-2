package chess;

import java.io.File;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;

public class ChessGame {

	//Planche de jeu (incluant les pi�ces)
	private ChessBoard board;
	
	public ChessBoard getBoard() {
		return board;
	}
	
	public void movePiece(String move) {
		String[] parts = move.split("-");
		board.move(ChessUtils.convertAlgebraicPosition(parts[0]), ChessUtils.convertAlgebraicPosition(parts[1]));
	}
	
	public boolean compareBoard(ChessBoard board) {
		return this.board.equals(board);
	}
	
	//Charge une planche de jeu à partir d'un fichier.
	public void loadBoard(String fileName) {
		try {
			board = ChessBoard.readFromFile(fileName);
		} catch (Exception e) {
			Alert alert = new Alert(AlertType.ERROR, "Error reading file", ButtonType.OK);
			alert.showAndWait();
			return;
		}
	}
	
	//Charge une planche de jeu à partir d'un fichier.
	public void loadBoard(File file, int boardPosX, int boardPosY) {
		try {
			board = ChessBoard.readFromFile(file, boardPosX, boardPosY);
		} catch (Exception e) {
			Alert alert = new Alert(AlertType.ERROR, "Error reading file", ButtonType.OK);
			alert.showAndWait();
			return;
		}
	}

	//Sauvegarde la planche de jeu actuelle dans un fichier.
	public void saveBoard(File file) {
		try {
			board.saveToFile(file);
		} catch (Exception e) {
			Alert alert = new Alert(AlertType.ERROR, "Error writing file", ButtonType.OK);
			alert.showAndWait();
			return;
		}
	}

	//Démarre l'enregistrement des mouvements du jeu dans un fichier de script.
	public void saveScript(File file) {
		try {
			throw new Exception("Pas implanté!");
		} catch (Exception e) {
			Alert alert = new Alert(AlertType.ERROR, "Error writing file", ButtonType.OK);
			alert.showAndWait();
			return;
		}
	}
	
	//Charge un fichier de script 
	public void loadScript(File file) {
		try {
			throw new Exception("Pas implanté!");
		} catch (Exception e) {
			Alert alert = new Alert(AlertType.ERROR, "Error reading file", ButtonType.OK);
			alert.showAndWait();
			return;
		}
	}
}
