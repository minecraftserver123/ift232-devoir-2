package chess.tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import chess.ChessGame;
import chess.ChessBoard;

class MoveTest {

	@Test
	public void testBasicCollision() throws Exception {
		ChessGame game = new ChessGame();
		game.loadBoard("boards/normalStart");
		ChessBoard result = ChessBoard.readFromFile("boards/normalStart"); //Move tower over a pawn of the same color
		game.movePiece("a1-a2");
		assertTrue(game.compareBoard(result));
	}
	
	@Test
	public void testPawn() throws Exception {
		ChessGame game = new ChessGame();
		
		game.loadBoard("boards/tests/pawn/base");
		ChessBoard result = ChessBoard.readFromFile("boards/tests/pawn/1downW1upB");
		game.movePiece("d4-d3");
		game.movePiece("f4-f5");
		assertFalse(game.compareBoard(result));
		
		game.loadBoard("boards/tests/pawn/base");
		result = ChessBoard.readFromFile("boards/tests/pawn/1upW1downB");
		game.movePiece("d4-d5");
		game.movePiece("f4-f3");
		assertTrue(game.compareBoard(result));
		
		game.loadBoard("boards/tests/pawn/base");
		result = ChessBoard.readFromFile("boards/tests/pawn/1rightWB");
		game.movePiece("d4-e4");
		game.movePiece("g4-g4");
		assertFalse(game.compareBoard(result));
	}
	
	@Test
	public void testBishop() throws Exception {
		ChessGame game = new ChessGame();
		
		game.loadBoard("boards/tests/bishop/base");
		ChessBoard result = ChessBoard.readFromFile("boards/tests/bishop/1down1left");
		game.movePiece("d4-c3");
		assertTrue(game.compareBoard(result));
		
		game.loadBoard("boards/tests/bishop/base");
		result = ChessBoard.readFromFile("boards/tests/bishop/1right");
		game.movePiece("d4-e4");
		assertFalse(game.compareBoard(result));
		
		game.loadBoard("boards/tests/bishop/base");
		result = ChessBoard.readFromFile("boards/tests/bishop/1up1right");
		game.movePiece("d4-e5");
		assertTrue(game.compareBoard(result));
	}
	
	@Test
	public void testKing() throws Exception {
		ChessGame game = new ChessGame();
		
		game.loadBoard("boards/tests/king/base");
		ChessBoard result = ChessBoard.readFromFile("boards/tests/king/1down1left");
		game.movePiece("d4-c3");
		assertTrue(game.compareBoard(result));
		
		game.loadBoard("boards/tests/king/base");
		result = ChessBoard.readFromFile("boards/tests/king/1right");
		game.movePiece("d4-e4");
		assertTrue(game.compareBoard(result));
		
		game.loadBoard("boards/tests/king/base");
		result = ChessBoard.readFromFile("boards/tests/king/1up");
		game.movePiece("d4-d5");
		assertTrue(game.compareBoard(result));
		
		game.loadBoard("boards/tests/king/base");
		result = ChessBoard.readFromFile("boards/tests/king/2up");
		game.movePiece("d4-d6");
		assertFalse(game.compareBoard(result));
	}
	
	@Test
	public void testKnight() throws Exception {
		ChessGame game = new ChessGame();
		
		game.loadBoard("boards/tests/knight/base");
		ChessBoard result = ChessBoard.readFromFile("boards/tests/knight/1left");
		game.movePiece("d4-c4");
		assertFalse(game.compareBoard(result));
		
		game.loadBoard("boards/tests/knight/base");
		result = ChessBoard.readFromFile("boards/tests/knight/2down1left");
		game.movePiece("d4-c2");
		assertTrue(game.compareBoard(result));
		
		game.loadBoard("boards/tests/knight/base");
		result = ChessBoard.readFromFile("boards/tests/knight/2up");
		game.movePiece("d4-d6");
		assertFalse(game.compareBoard(result));
		
		game.loadBoard("boards/tests/knight/base");
		result = ChessBoard.readFromFile("boards/tests/knight/2up1right");
		game.movePiece("d4-e6");
		assertTrue(game.compareBoard(result));
	}
	
	@Test
	public void testQueen() throws Exception {
		ChessGame game = new ChessGame();
		
		game.loadBoard("boards/tests/queen/base");
		ChessBoard result = ChessBoard.readFromFile("boards/tests/queen/1down2right");
		game.movePiece("d4-f3");
		assertFalse(game.compareBoard(result));
		
		game.loadBoard("boards/tests/queen/base");
		result = ChessBoard.readFromFile("boards/tests/queen/2down2left");
		game.movePiece("d4-b2");
		assertTrue(game.compareBoard(result));
		
		game.loadBoard("boards/tests/queen/base");
		result = ChessBoard.readFromFile("boards/tests/queen/2right");
		game.movePiece("d4-f4");
		assertTrue(game.compareBoard(result));
		
		game.loadBoard("boards/tests/queen/base");
		result = ChessBoard.readFromFile("boards/tests/queen/2up");
		game.movePiece("d4-d6");
		assertTrue(game.compareBoard(result));
	}
	
	@Test
	public void testRook() throws Exception {
		ChessGame game = new ChessGame();
		
		game.loadBoard("boards/tests/rook/base");
		ChessBoard result = ChessBoard.readFromFile("boards/tests/rook/2left");
		game.movePiece("d4-b4");
		assertTrue(game.compareBoard(result));
		
		game.loadBoard("boards/tests/rook/base");
		result = ChessBoard.readFromFile("boards/tests/rook/2up");
		game.movePiece("d4-d6");
		assertTrue(game.compareBoard(result));
		
		game.loadBoard("boards/tests/rook/base");
		result = ChessBoard.readFromFile("boards/tests/rook/2up2right");
		game.movePiece("d4-f6");
		assertFalse(game.compareBoard(result));
	}
}
