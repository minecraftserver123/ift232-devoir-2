package chess.ui;

import java.io.File;

import chess.ChessGame;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

public class GameView extends Application {
	
	//Dialogue utilis� pour choisir des noms de fichiers
	private FileChooser fileDialog;
	
	//Taille de la fenêtre
	private int gameSizeX = 1200;
	private int gameSizeY = 1000;
	
	//Position de l'échiquier dans la fenêtre
	private int boardPosX = 200;
	private int boardPosY = 100;

	//Objet racine de l'interface graphique
	private Scene scene;
	
	//Panneau principal dans lequel se trouvent les éléments de jeu
	private Pane gamePane;
	
	private ChessGame cg;
	
	@Override
	public void start(Stage stage) {
		
		cg = new ChessGame();
		gamePane = new Pane();
		
		fileDialog = new FileChooser();
		
		resetGame();
		
		//Bouton de redémarrage (recharge la planche de jeu par défaut)
		Button resetButton = new Button("Reset");
		resetButton.setLayoutX(250);
		resetButton.setLayoutY(50);
		gamePane.getChildren().add(resetButton);

		resetButton.setOnAction(event -> resetGame());

		
		//Bouton utilisé pour enregistrer les mouvements dnas un script
		Button recordButton = new Button("Record moves");
		recordButton.setLayoutX(325);
		recordButton.setLayoutY(50);
		gamePane.getChildren().add(recordButton);
		recordButton.setOnAction(event -> {

			File file = getSaveFile("Record moves...", "scripts/saves", stage);
			cg.saveScript(file);
		});

		//Bouton utilisé pour sauvegarder la planche de jeu
		Button saveButton = new Button("Save board");
		saveButton.setLayoutX(425);
		saveButton.setLayoutY(50);
		gamePane.getChildren().add(saveButton);

		saveButton.setOnAction(event -> {

			File file = getSaveFile("Save Board...", "boards/saves", stage);
			cg.saveBoard(file);
		});

		//Boîte de sélection utilisée pour activer l'intelligence artificielle
		CheckBox aiBox = new CheckBox("AI player");
		aiBox.setLayoutX(525);
		aiBox.setLayoutY(50);

		gamePane.getChildren().add(aiBox);

		
		//Bouton utilisé pour charger une planche de jeu
		Button loadButton = new Button("Load board");
		loadButton.setLayoutX(625);
		loadButton.setLayoutY(50);
		gamePane.getChildren().add(loadButton);

		loadButton.setOnAction(event -> {

			File file = getOpenFile("Open Board...", "boards", stage);

			resetGame(file);
		});

		
		//Bouton utilisé pour charger et exécuter une ancienne partie
		Button playButton = new Button("Play moves");
		playButton.setLayoutX(725);
		playButton.setLayoutY(50);
		gamePane.getChildren().add(playButton);

		playButton.setOnAction(event -> {
			File file = getOpenFile("Open Script...", "scripts", stage);
			cg.loadScript(file);
		});

		//Bouton undo, utilisé pour défaire le dernier mouvement
		Button undoButton = new Button("Undo");
		undoButton.setLayoutX(825);
		undoButton.setLayoutY(50);
		gamePane.getChildren().add(undoButton);
		
		//Bouton redo, utilisé pour refaire un mouvement défait
		Button redoButton = new Button("Redo");
		redoButton.setLayoutX(900);
		redoButton.setLayoutY(50);
		gamePane.getChildren().add(redoButton);

		
		//Boîte de sélection utilisée pour activer l'édition d'échiquier (déposer manuellement des pièces)
		CheckBox editBox = new CheckBox("Edit board");
		editBox.setLayoutX(200);
		editBox.setLayoutY(950);

		gamePane.getChildren().add(editBox);

		//Étiquette pour la zone de capture des noirs
		Label blackCapture = new Label("Black captures");
		blackCapture.setLayoutX(50);
		blackCapture.setLayoutY(200);
		gamePane.getChildren().add(blackCapture);

		//Étiquette pour la zone de capture des blancs
		Label whiteCapture = new Label("White captures");
		whiteCapture.setLayoutX(1050);
		whiteCapture.setLayoutY(200);
		gamePane.getChildren().add(whiteCapture);

		//Préparation de la fenêtre principale
		scene = new Scene(gamePane, gameSizeX, gameSizeY);

		stage.setTitle("Super Mega Chess 3000");

		stage.setScene(scene);

		stage.show();
	}
	
	public static void main(String[] args) {
		launch(args);
	}
	
	//Retire la planche de jeu de la fenêtre
	private void clearGame() {
		if (cg.getBoard() != null) {
			gamePane.getChildren().remove(cg.getBoard().getUI());
		}
	}
	
	// Utilisé pour obtenir un dialogue d'ouverture
	private File getOpenFile(String title, String baseDir, Stage stage) {

		fileDialog.setTitle(title);
		fileDialog.setInitialDirectory(new File(System.getProperty("user.dir") + "/" + baseDir));
		return fileDialog.showOpenDialog(stage);
	}

	// Utilisé pour obtenir un dialogue de sauvegarde
	private File getSaveFile(String title, String baseDir, Stage stage) {

		fileDialog.setTitle(title);
		fileDialog.setInitialDirectory(new File(System.getProperty("user.dir") + "/" + baseDir));
		return fileDialog.showSaveDialog(stage);
	}
	
	private void resetGame() {
		File file = new File("boards/normalStart");
		resetGame(file);
	}
	
	private void resetGame(File file) {
		clearGame();
		cg.loadBoard(file, boardPosX, boardPosY);
		gamePane.getChildren().add(cg.getBoard().getUI());
		cg.getBoard().getUI().toBack();
	}
}
