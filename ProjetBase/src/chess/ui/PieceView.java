package chess.ui;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

import chess.ChessBoard;
import chess.ChessUtils;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;

public class PieceView
{
    // Utilisé pour générer les noms de fichiers contenant les images des pièces.
    private static final String names[] = { "pawn", "knight", "bishop", "rook", "queen", "king" };
    private static final String prefixes[] = { "w", "b" };
    
    // Taille d'une pièce dans l'interface
    private static double pieceSize = 75.0;
    
    // Panneau d'interface contenant l'image de la pièce
    private Pane piecePane;
    
    // Référence à la planche de jeu. Utilisée pour déplacer la pièce.
    private ChessBoard board;
    
    public PieceView(ChessBoard b) {
        board = b;
    }
    
    // Création d'une pièce normale. La position algébrique en notation d'échecs
    // lui donne sa position sur la grille.
    public PieceView(String name, ChessBoard b) {
    	
        board = b;
        
        Image pieceImage;
        try {
            pieceImage = new Image(new FileInputStream("images/" + prefixes[ChessUtils.getColor(name)] + names[ChessUtils.getType(name)] + ".png"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return;
        }
        ImageView pieceView = new ImageView(pieceImage);

        pieceView.setX(0);
        pieceView.setY(0);
        
        pieceView.setFitHeight(pieceSize);
        pieceView.setFitWidth(pieceSize);

        pieceView.setPreserveRatio(true);
        piecePane = new Pane(pieceView);
        enableDragging(piecePane);
    }
    
    
    
    // Gestionnaire d'événements pour le déplacement des pièces
    private void enableDragging(Node node) {
        final ObjectProperty<Point2D> mouseAnchor = new SimpleObjectProperty<>();
        final ObjectProperty<Point2D> pos = new SimpleObjectProperty<>();
        final ObjectProperty<Point2D> newPos = new SimpleObjectProperty<>();

        // Lorsque la pièce est saisie, on préserve la position de départ
        node.setOnMousePressed(event -> {
            mouseAnchor.set(new Point2D(event.getSceneX(), event.getSceneY()));
            pos.set(mouseAnchor.get());
        });

        // À chaque événement de déplacement, on déplace la pièce et on met à
        // jour la position de départ
        node.setOnMouseDragged(event -> {
            double deltaX = event.getSceneX() - mouseAnchor.get().getX();
            double deltaY = event.getSceneY() - mouseAnchor.get().getY();
            node.relocate(node.getLayoutX() + deltaX, node.getLayoutY() + deltaY);
            node.toFront();
            mouseAnchor.set(new Point2D(event.getSceneX(), event.getSceneY()));

        });

        // Lorsqu'on relâche la pièce, le mouvement correspondant est appliqué
        // au jeu d'échecs si possible.
        // L'image de la pièce est également centrée sur la case la plus proche.
        node.setOnMouseReleased(event -> {
            newPos.set(new Point2D(event.getSceneX(), event.getSceneY()));
            board.move(pos.get(), newPos.get());
        });
    }
    
    public Pane getPane() {
        return piecePane;
    }
    
    public void move(Point2D pos) {
        piecePane.relocate(pos.getX(), pos.getY());
    }
}
