package chess;

import java.awt.Point;

import chess.ui.PieceView;
import javafx.geometry.Point2D;
import javafx.scene.layout.Pane;

public class ChessPiece {
	private int type;
	private int color;
	
    // Position de la piÃ¨ce sur l'Ã©chiquier
    private int gridPosX;
    private int gridPosY;
    
    private PieceView pv;

	// Pour créer des pièces à mettre sur les cases vides
	public ChessPiece(int x, int y, ChessBoard b) {
		gridPosX = x;
		gridPosY = y;
		this.type = ChessUtils.TYPE_NONE;
		this.color = ChessUtils.COLORLESS;
		pv = new PieceView(b);
	}

	// Création d'une pièce normale. La position algébrique en notation d'échecs
	// lui donne sa position sur la grille.
	public ChessPiece(String name, String pos, ChessBoard b) {
		color = ChessUtils.getColor(name);
		type = ChessUtils.getType(name);
		
		pv = new PieceView(name, b);
		setAlgebraicPos(pos);
	}
	
	public static ChessPiece readFromStream(String line, ChessBoard board) {
        String[] parts;
        parts = line.split("-");
        return new ChessPiece(parts[1], parts[0], board);
	}
	
	public String saveToStream() {
		String value;
		try {
			value = ChessUtils.makeAlgebraicPosition(getGridX(), getGridY()) + "-" + ChessUtils.makePieceName(color, type);
		} catch (Exception e) {
			value = "";
		}
	    return value;
    }
	
	//Change la position avec la notation algÃ©brique
    public void setAlgebraicPos(String pos) {
        Point pos2d = ChessUtils.convertAlgebraicPosition(pos);

        gridPosX = pos2d.x;
        gridPosY = pos2d.y;
    }
    
    public boolean verifyMove(Point startPos,Point endPos) {
    	int deltaX = endPos.x - startPos.x;
    	int deltaY = endPos.y - startPos.y;
    	int aDeltaX = Math.abs(deltaX);
    	int aDeltaY = Math.abs(deltaY);
    	
    	if(aDeltaX != 0 || deltaY != 0){
    		switch(type) {
	    		case ChessUtils.TYPE_BISHOP: {
	    			if(aDeltaX == aDeltaY) return true;
	    			break;
	    		}
	    		case ChessUtils.TYPE_KING: {
	    			if((aDeltaX == 0 || aDeltaX == 1) && (aDeltaY == 0 || aDeltaY == 1)) return true;
	    			break;
	    		}
				case ChessUtils.TYPE_KNIGHT: {
					if(aDeltaX == 1 && aDeltaY == 2) return true;
					if(aDeltaX == 2 && aDeltaY == 1) return true;
					break;
	    		}
				case ChessUtils.TYPE_PAWN: {
					if(deltaX != 0) break;
					if(deltaY != 1 && color == ChessUtils.BLACK) break;
					if(deltaY != -1 && color == ChessUtils.WHITE) break;
					return true;
				}
				case ChessUtils.TYPE_QUEEN: {
					if(aDeltaX == 0 && aDeltaY >= 1 && aDeltaY <= 7) return true;
					if(aDeltaY == 0 && aDeltaX >= 1 && aDeltaX <= 7) return true;
					if(aDeltaX == aDeltaY) return true;
					break;
				}
				case ChessUtils.TYPE_ROOK: {
					if(aDeltaX == 0 && aDeltaY >= 1 && aDeltaY <= 7) return true;
					if(aDeltaY == 0 && aDeltaX >= 1 && aDeltaX <= 7) return true;
					break;
				}
				default: {
					return false;
				}
    		}
    	}
    	
		return false;
    }
	
	//Pour savoir si c'est une pièce vide (pour les cases vides de l'échiquier).
	public boolean isNone() {
		return type == ChessUtils.TYPE_NONE;
	}

	//Accesseurs divers
	public int getType() {
		return type;
	}

	public int getColor() {
		return color;
	}
	
	public Pane getUI() {
	    return pv.getPane();
	}
	
	public int getGridX() {
	    return gridPosX;
	}
	
	public int getGridY() {
        return gridPosY;
    }
	
	public void move(Point2D pos) {
	    pv.move(pos);
	}
	
	public void setGridPos(Point pos) {
		gridPosX = pos.x;
		gridPosY = pos.y;
	}
	
	@Override
    public boolean equals(Object obj) {
		if((obj == null) || (getClass() != obj.getClass())){
            return false;
        }

        final ChessPiece other = (ChessPiece) obj;
        if (this.type != other.type && this.color != other.color || 
        		this.gridPosX != other.gridPosX || this.gridPosY != other.gridPosY) {
            return false;
        }

        return true;
    }
}
